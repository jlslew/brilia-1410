<?php

return [
    [
        'text' => 'The hottest new branding shop in town. Keep an eye on this company!',
        'source' => 'Jodie McNamara, Pluck - 1',
    ],
    [
        'text' => 'The hottest new branding shop in town. Keep an eye on this company!',
        'source' => 'Jodie McNamara, Pluck - 2',
    ],
];
