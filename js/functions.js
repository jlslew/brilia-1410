+function($) {
    'use strict';

    var siteurl = $('body').data('siteurl');

    $.getScript('//use.typekit.net/smd8dws.js', function() {
        var inactive = function() {
            $.get(siteurl + '/futura-pt.css', function(css) {
                $('head').append('<style>' + css + '</style>');
            });
        };

        Typekit.load({inactive: inactive});
    });

    $(function() {
        $.when($('#splash').delay(10000).slideUp(1000)).then(function() {
            $('#main').removeClass('hide').animate({marginTop: 0}, 800, function() {
                Waypoint.refreshAll();
            });

            $.getScript('//platform.twitter.com/widgets.js', function() {
                twttr.events.bind('rendered', function() {
                    $('.twitter-timeline').contents().find('head link').attr('href', siteurl + '/twitter.css');
                });

                $('.twitter-timeline').attr('height', function() {
                    return $(this).parent().height();
                });
            });

            $('#contact .form-control').each(function() {
                var $this = $(this);

                $this.data('text', $this.val().replace(/<br>/g, '\n'));
                $this.val($this.data('text'));

                $this.focus(function() {
                    if ($this.data('text') === $this.val())
                        $this.val('');
                }).blur(function() {
                    if ($this.val() === '')
                        $this.val($this.data('text'));
                });
            });
        });
    });

    $('.navbar a').click(function() {
        $('html, body').animate({
            scrollTop: $(this.hash).offset().top
        }, 1200);
    });

    (function() {
        $('#main').css('marginTop', $(window).height());
        $('.vh').css('height', $(window).height());
        $('#home').css('height', $(window).height() - $('.navbar').height());

        var part = window.matchMedia('(max-width:767px)').matches ? 4 : 10;

        $('.cell-anim').each(function() {
            var img = new Image();
            img.src = $(this).data('src');
            $(this).parent().data('img', img);
        });

        $.each([1, 2, 3, 4, 5, 6, 7, 8, 9], function(i) {
            $('.cell-' + i)
                    .css('minHeight', Math.floor(i * $(window).width() / part))
                    .each(function() {
                        var timeout, $this = $(this);

                        var show = function() {
                            $this.removeClass('cell-hide').addClass('cell-show');

                            $this.find('.cell-anim').each(function() {
                                clearTimeout(timeout);
                                $(this).attr('src', $this.data('img').src);
                            });
                        };
                        var hide = function() {
                            $this.removeClass('cell-show').addClass('cell-hide');

                            timeout = setTimeout(function() {
                                $this.find('.cell-anim').each(function() {
                                    $(this).attr('src', '');
                                });
                            }, 600);
                        };

                        $this.waypoint(function(dir) {
                            if (dir === 'down')
                                show();
                            else
                                hide();
                        }, {
                            offset: function() {
                                return $(window).height() - ($this.height() / 3);
                            }
                        });
                        $this.waypoint(function(dir) {
                            if (dir === 'down')
                                hide();
                            else
                                show();
                        }, {
                            offset: function() {
                                return -$this.height() * 2 / 3;
                            }
                        });
                    });
        });
    }());
}(jQuery);