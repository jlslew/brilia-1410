#!/bin/bash

set -e

LESSC='lessc --strict-math=on --include-path=. --compress'

if which lessc; then
	$LESSC less/futura-pt.less > futura-pt.css
	$LESSC less/bootstrap.less > brilia.css
	$LESSC less/twitter.less > twitter.css
else
	echo 'Error: lessc not found. Install Node.js then: npm install -g less';
	exit 1;
fi

if which uglifyjs; then
	node js/bootstrap.js; uglifyjs brilia.js -nco brilia.js
else
	echo 'Error: uglifyjs not found. Install Node.js then: npm install -g uglify-js';
	exit 1;
fi
