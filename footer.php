<footer class="container" role="contentinfo">
    <section class="row">
        <div class="col-sm-12 text-center">
            <p><a href="{{ wp.esc_url(wp.__('//wordpress.org', 'brilia')) }}">{% do wp.printf(wp.__('Proudly powered by %s', 'brilia'), 'WordPress') %}</a></p>
        </div>
    </section>
</footer>
{% do wp.wp_footer() %}
</body>
</html>