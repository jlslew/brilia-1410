<?php

function display_quotes() {
    static $quotes = [];

    if (empty($quotes)) {
        $quotes = require_once __DIR__ . '/../quotes.php';
        shuffle($quotes);
    }

    $quote = array_shift($quotes);
    return "<p>{$quote['text']}</p><cite>{$quote['source']}</cite>";
}

function display_portfolios() {
    static $portfolios = [];

    if (empty($portfolios)) {
        $portfolios = glob(__DIR__ . '/../portfolios/*');
        shuffle($portfolios);
    }

    return 'background-image:url(' . get_template_directory_uri() . '/portfolios/'
        . basename(array_shift($portfolios)) . ')';
}
