<?php

require_once 'vendor/autoload.php';

/**
 * Set up theme defaults and registers support for various WordPress features.
 */
add_action('after_setup_theme', function () {
    // Make Twenty Fourteen available for translation.
    load_theme_textdomain('brilia', get_template_directory() . '/lang');

    // Add RSS feed links to <head> for posts and comments.
    add_theme_support('automatic-feed-links');

    // Enable support for Post Thumbnails, and declare two sizes.
    add_theme_support('post-thumbnails');

    /*
     * Switch default core markup for search form, comment form, and comments to
     * output valid HTML5.
     */
    add_theme_support('html5', [
        'comment-list', 'comment-form', 'search-form', 'gallery', 'caption'
    ]);
});

add_action('template_include', function ($filename) {
    $loader = new Twig_Loader_Chain();
    $loader->addLoader(new Twig_Loader_Filesystem(__DIR__));
    $loader->addLoader(new Twig_Loader_String());

    $twig = new Twig_Environment($loader, ['cache' => false]);

    require_once 'inc/twig-proxy.php';
    $wp = new Twig_Proxy($twig);

    $twig->addGlobal('wp', $wp);
    $twig->loadTemplate(basename($filename))->display([]);
});

/**
 * Enqueue scripts and styles for the front end.
 */
add_action('wp_enqueue_scripts', function () {
    // Load our main stylesheet.
    wp_enqueue_style('brilia-style', get_template_directory_uri() . '/brilia.css');

    wp_enqueue_script('brilia-script', get_template_directory_uri() . '/brilia.js', ['jquery'], '', true);
});

add_filter('clean_url', function ($url) {
    if (strpos($url, 'ver=')) {
        $url = remove_query_arg('ver', $url);
    }

    return strpos($url, '.js') ? "$url' defer='defer" : $url;
}, 11, 1);

/**
 * Create a nicely formatted and more specific title element text for output in
 * head of document, based on current view.
 *
 * @global int $paged WordPress archive pagination page count.
 * @global int $page  WordPress paginated post page count.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
add_filter('wp_title', function ( $title, $sep ) {
    global $paged, $page;

    if (!is_feed()) {
        // Add the site name.
        $title .= get_bloginfo('name', 'display');

        // Add the site description for the home/front page.
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && ( is_home() || is_front_page() )) {
            $title = "$title $sep $site_description";
        }

        // Add a page number if necessary.
        if (( $paged >= 2 || $page >= 2 ) && !is_404()) {
            $title = "$title $sep " . sprintf(__('Page %s', 'twentyfourteen'), max($paged, $page));
        }
    }

    return $title;
}, 10, 2);

show_admin_bar(false);

// Custom template tags for this theme.
require get_template_directory() . '/inc/template-tags.php';
