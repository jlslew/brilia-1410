{% spaceless %}
    {% do wp.get_template_part('header') %}
    <main id="main" class="container-fluid hide" role="main">
        <section id="home" class="row vh cell-transition">
            <h3 class="col-sm-offset-4 heading">
                <span class="line line-blue">{% do wp._e('SUPER', 'brilia') %}</span>
                <span class="line line-yellow">{% do wp._e('NOVA', 'brilia') %}</span>
                <span class="line line-red">{% do wp._e('HOT', 'brilia') %}</span>
                <span class="line">{% do wp._e('DESIGNY', 'brilia') %}</span>
                <span class="line">{% do wp._e('STUFF', 'brilia') %}</span>
            </h3>
            <div class="col-sm-3 col-sm-offset-4 text">
                <p>{% do wp._e('Through the art of storytelling and a passion for ground-breaking design, we create compelling brands with stories that resonate with their audience.', 'brilia') %}</p>
                <p>{% do wp._e('Use the scroll bar, the rolly ball on your mouse, or the menu at the top of our page to discover more about Brilia.', 'brilia') %}</p>
            </div>
        </section>

        <section id="work" class="row">
            <div class="col-sm-5 col-md-2">
                <div class="col-sm-10 cell cell-3 hidden-sm"></div>
                <div class="col-sm-10 cell cell-2 cell-work" style="{{ wp.display_portfolios() }}"></div>
                <div class="col-sm-10 cell cell-2 cell-work" style="{{ wp.display_portfolios() }}"></div>
            </div>
            <div class="col-sm-5 col-md-2">
                <div class="col-sm-10 cell cell-1 hidden-sm"></div>
                <div class="col-sm-10 cell cell-3 cell-work" style="{{ wp.display_portfolios() }}"></div>
                <div class="col-sm-10 cell cell-2 cell-studio">
                    <header class="cell-header">
                        <h3 class="cell-title">{% do wp._e('A full service studio.', 'brilia') %}</h3>
                    </header>
                    <div class="cell-body">
                        <p>{% do wp._e('From websites to print and advertising to branding, we offer a wide range of creative services.', 'brilia') %}</p>
                        <p>{% do wp._e('Click here to see what we can do for you.', 'brilia') %}</p>
                    </div>
                    <footer class="cell-footer">
                        <a class="btn btn-studio" href="#"></a>
                    </footer>
                </div>
                <div class="col-sm-10 cell cell-2 cell-work" style="{{ wp.display_portfolios() }}"></div>
            </div>
            <div class="col-sm-10 col-md-4">
                <div class="col-sm-10 cell cell-4 cell-design">
                    <img class="cell-anim img-responsive" data-src="{{ wp.get_template_directory_uri() }}/img/design-animation.gif" />
                    <header class="cell-header">
                        <h2 class="cell-title">{% do wp._e('Our clients, our passion.', 'brilia') %}</h2>
                    </header>
                    <div class="cell-body">
                        <p>{% do wp._e('We put our heart and soul into conceiving the best creative solutions for every client.  By tapping into new technologies and old experiences, we bring a professionalism and playfulness to our work.', 'brilia') %}</p>
                    </div>
                    <footer class="cell-footer">
                        <button class="btn btn-design">{% do wp._e('VIEW PORTFOLIO', 'brilia') %}</button>
                    </footer>
                </div>
                <div class="col-sm-5">
                    <div class="col-sm-10 cell cell-3 cell-work" style="{{ wp.display_portfolios() }}"></div>
                    <div class="col-sm-10 cell cell-2 cell-work" style="{{ wp.display_portfolios() }}"></div>
                </div>

                <div class="col-sm-5">
                    <div class="col-sm-10 cell cell-2 cell-work" style="{{ wp.display_portfolios() }}"></div>
                    <blockquote class="col-sm-10 cell cell-2">{{ wp.display_quotes() | raw }}</blockquote>
                </div>
            </div>
            <div class="col-sm-5 col-md-2">
                <div class="col-sm-10 cell cell-3 hidden-sm"></div>
                <div class="col-sm-10 cell cell-2 cell-work" style="{{ wp.display_portfolios() }}"></div>
                <div class="col-sm-10 cell cell-2 cell-work" style="{{ wp.display_portfolios() }}"></div>
            </div>
        </section>

        <section class="row vh cell-transition">
            <h3 class="col-sm-offset-4 heading">
                <span class="line">{% do wp._e('A&nbsp;<span class="line-red">SIZZLING</span>', 'brilia') %}</span>
                <span class="line">{% do wp._e('<span class="line-yellow">ARRAY</span>&nbsp;OF', 'brilia') %}</span>
                <span class="line line-green">{% do wp._e('CREATIVE', 'brilia') %}</span>
                <span class="line">{% do wp._e('SERVICES', 'brilia') %}</span>
            </h3>
            <div class="col-sm-4 col-sm-offset-4 text">
                <p>{% do wp._e('Through the art of storytelling and a passion for ground-breaking design, we create compelling brands with stories that resonate with their audience.', 'brilia') %}</p>
                <p>{% do wp._e('Use the scroll bar, the rolly ball on your mouse, or the menu at the top of our page to discover the resplendent quality of our work.', 'brilia') %}</p>
            </div>
        </section>

        <section id="services" class="row">
            <div class="col-sm-5 col-md-2">
                <div class="col-sm-10 cell cell-1 hidden-sm"></div>
                <blockquote class="col-sm-10 cell cell-2">{{ wp.display_quotes() | raw }}</blockquote>
                <figure class="col-sm-10 cell cell-2 cell-market-research">
                    <div class="cell-icon"></div>
                    <figcaption class="cell-caption">
                        <h3 class="title">{% do wp._e('MARKET RESEARCH', 'brilia') %}</h3>
                        <p>{% do wp._e('We focus your brand to better suit your target demographic.', 'brilia') %}</p>
                    </figcaption>
                </figure>
                <figure class="col-sm-10 cell cell-2 cell-graphic-design">
                    <div class="cell-icon"></div>
                    <figcaption class="cell-caption">
                        <h3 class="title">{% do wp._e('GRAPHIC DESIGN', 'brilia') %}</h3>
                        <p>{% do wp._e('From logos to charts - we ensure your brand can compete in today&#39;s market.', 'brilia') %}</p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-sm-10 col-md-4">
                <div class="col-sm-10 cell cell-4 cell-inspire">
                    <img class="cell-anim img-responsive" data-src="{{ wp.get_template_directory_uri() }}/img/inspire-animation.gif" />
                    <header class="cell-header">
                        <h2 class="cell-title">{% do wp._e('Our work, our passion.', 'brilia') %}</h2>
                    </header>
                    <div class="cell-body">
                        <p>{% do wp._e('We put our heart and soul into conceiving the best creative solutions for every client.  By tapping into new and old experiences, we bring a playfulness to our creative process.', 'brilia') %}</p>
                    </div>
                    <footer class="cell-footer">
                        <button class="btn btn-inspire">{% do wp._e('VIEW SERVICES', 'brilia') %}</button>
                    </footer>
                </div>
                <figure class="col-sm-5 cell cell-2 cell-exhibit-design">
                    <div class="cell-icon"></div>
                    <figcaption class="cell-caption">
                        <h3 class="title">{% do wp._e('EXHIBIT DESIGN', 'brilia') %}</h3>
                        <p>{% do wp._e('We provide the tools for trade shows to make your brand stand out.', 'brilia') %}</p>
                    </figcaption>
                </figure>
                <figure class="col-sm-5 cell cell-2 cell-marketing">
                    <div class="cell-icon"></div>
                    <figcaption class="cell-caption">
                        <h3 class="title">{% do wp._e('MARKETING', 'brilia') %}</h3>
                        <p>{% do wp._e('Focus your market value by targeting consumers with innovative ideas.', 'brilia') %}</p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-sm-5 col-md-2">
                <div class="col-sm-10 cell cell-1 hidden-sm"></div>
                <figure class="col-sm-10 cell cell-2 cell-brand-development">
                    <div class="cell-icon"></div>
                    <figcaption class="cell-caption">
                        <h3 class="title">{% do wp._e('BRAND DEVELOPMENT', 'brilia') %}</h3>
                        <p>{% do wp._e('Whether it&#39;s a fresh start or a rebrand, Brilia can make a unique statement.', 'brilia') %}</p>
                    </figcaption>
                </figure>
                <div class="col-sm-10 cell cell-2 cell-studio">
                    <header class="cell-header">
                        <h3 class="cell-title">{% do wp._e('A full service studio.', 'brilia') %}</h3>
                    </header>
                    <div class="cell-body">
                        <p>{% do wp._e('From websites to print and advertising to branding, we offer a wide range of creative services.', 'brilia') %}</p>
                        <p>{% do wp._e('Click here to see what we can do for you.', 'brilia') %}</p>
                    </div>
                    <footer class="cell-footer">
                        <a class="btn btn-studio" href="#"></a>
                    </footer>
                </div>
                <figure class="col-sm-10 cell cell-2 cell-printing">
                    <div class="cell-icon"></div>
                    <figcaption class="cell-caption">
                        <h3 class="title">{% do wp._e('PRINTING', 'brilia') %}</h3>
                        <p>{% do wp._e('High quality printing services for any type of private or commercial project.', 'brilia') %}</p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-sm-5 col-md-2">
                <div class="col-sm-10 cell cell-2 hidden-sm"></div>
                <figure class="col-sm-10 cell cell-2 cell-web-development">
                    <div class="cell-icon"></div>
                    <figcaption class="cell-caption">
                        <h3 class="title">{% do wp._e('WEB DEVELOPMENT', 'brilia') %}</h3>
                        <p>{% do wp._e('The websites we produce will embody the interactive, the professional, and the unique all in one.', 'brilia') %}</p>
                    </figcaption>
                </figure>
                <figure class="col-sm-10 cell cell-2 cell-social-media">
                    <div class="cell-icon"></div>
                    <figcaption class="cell-caption">
                        <h3 class="title">{% do wp._e('SOCIAL MEDIA', 'brilia') %}</h3>
                        <p>{% do wp._e('We believe in social media that captivates your followers every step of the way.', 'brilia') %}</p>
                    </figcaption>
                </figure>
                <figure class="col-sm-10 cell cell-2 cell-writing-edting">
                    <div class="cell-icon"></div>
                    <figcaption class="cell-caption">
                        <h3 class="title">{% do wp._e('WRITING AND EDITING', 'brilia') %}</h3>
                        <p>{% do wp._e('Creative or technical, marketing campaigns to annual reports, we do it all!', 'brilia') %}</p>
                    </figcaption>
                </figure>
            </div>
        </section>

        <section class="row vh cell-transition">
            <h3 class="col-sm-offset-4 heading">
                <span class="line line-blue">{% do wp._e('SMOKIN', 'brilia') %}</span>
                <span class="line line-blue">{% do wp._e('HOT&nbsp;NEWS', 'brilia') %}</span>
                <span class="line">{% do wp._e('FROM&nbsp;THE', 'brilia') %}</span>
                <span class="line">{% do wp._e('FEED', 'brilia') %}</span>
            </h3>
            <div class="col-sm-4 col-sm-offset-4 text">
                <p>{% do wp._e('Through the art of storytelling and a passion for ground-breaking design, we create compelling brands with stories that resonate with their audience.', 'brilia') %}</p>
                <p>{% do wp._e('Use the scroll bar, the rolly ball on your mouse, or the menu at the top of our page to discover the resplendent quality of our work.', 'brilia') %}</p>
            </div>
        </section>

        <section id="connect" class="row">
            <div class="col-sm-5 col-md-2"></div>
            <div class="col-sm-5 col-md-2"></div>
            <div class="col-sm-10 col-md-4">
                <div class="col-sm-10 cell cell-4 cell-explore">
                    <img class="cell-anim img-responsive" data-src="{{ wp.get_template_directory_uri() }}/img/explore-animation.gif" />
                    <header class="cell-header">
                        <h2 class="cell-title">{% do wp._e('Our work, our passion.', 'brilia') %}</h2>
                    </header>
                    <div class="cell-body">
                        <p>{% do wp._e('We put our heart and soul into conceiving the best creative solutions for every client.  By tapping into new and old experiences, we bring a playfulness to our creative process.', 'brilia') %}</p>
                    </div>
                    <footer class="cell-footer">
                        <button class="btn btn-block btn-explore">{% do wp._e('TYPE YOUR TWEET TO BRILIA HERE', 'brilia') %}</button>
                    </footer>
                </div>
            </div>
            <div class="col-sm-5 col-md-2">
                <div class="col-sm-10 cell cell-6">
                    <a class="twitter-timeline" data-border-color="#ffffff" data-chrome="noheader nofooter noscrollbar" data-link-color="#f9d800" data-widget-id="569566417121341440" href="//twitter.com/BriliaCreative">Tweets by @BriliaCreative</a>
                </div>
            </div>
        </section>

        <section class="row vh cell-transition">
            <h3 class="col-sm-offset-4 heading">
                <span class="line line-blue">{% do wp._e('SMOKIN', 'brilia') %}</span>
                <span class="line line-blue">{% do wp._e('HOT&nbsp;NEWS', 'brilia') %}</span>
                <span class="line">{% do wp._e('FROM&nbsp;THE', 'brilia') %}</span>
                <span class="line">{% do wp._e('FEED', 'brilia') %}</span>
            </h3>
            <div class="col-sm-4 col-sm-offset-4 text">
                <p>{% do wp._e('Through the art of storytelling and a passion for ground-breaking design, we create compelling brands with stories that resonate with their audience.', 'brilia') %}</p>
                <p>{% do wp._e('Use the scroll bar, the rolly ball on your mouse, or the menu at the top of our page to discover the resplendent quality of our work.', 'brilia') %}</p>
            </div>
        </section>

        <section id="contact" class="row">
            <div class="col-sm-5 col-md-2">
                <div class="col-sm-10 cell cell-2 hidden-sm"></div>
                <figure class="col-sm-10 cell cell-2 cell-tumblr">
                    <div class="cell-icon"></div>
                    <figcaption class="cell-caption">
                        <h3 class="title sr-only">{% do wp._e('TUMBLR', 'brilia') %}</h3>
                        <p><a href="//briliacreative.tumblr.com">briliacreative.tumblr.com</a></p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-sm-5 col-md-2">
                <div class="col-sm-10 cell cell-1 hidden-sm"></div>
                <figure class="col-sm-10 cell cell-2 cell-facebook">
                    <div class="cell-icon"></div>
                    <figcaption class="cell-caption">
                        <h3 class="title sr-only">{% do wp._e('FACEBOOK', 'brilia') %}</h3>
                        <p><a href="//facebook.com/BriliaCreative">facebook.com/BriliaCreative</a></p>
                    </figcaption>
                </figure>
                <figure class="col-sm-10 cell cell-2 cell-twitter">
                    <div class="cell-icon"></div>
                    <figcaption class="cell-caption">
                        <h3 class="title sr-only">{% do wp._e('TWITTER', 'brilia') %}</h3>
                        <p><a href="//twitter.com/BriliaCreative">twitter.com/BriliaCreative</a></p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-sm-10 col-md-4">
                <div class="col-sm-10 cell cell-7 cell-engage">
                    <img class="cell-anim img-responsive" data-src="{{ wp.get_template_directory_uri() }}/img/engage-animation.gif" />
                    <header class="cell-header">
                        <h2 class="cell-title">{% do wp._e('Get in Touch!', 'brilia') %}</h2>
                    </header>
                    <div class="cell-body">
                        <p>{% do wp._e('We&#39;re very approachable and would love to speak to you.  Feel free to call, send us an email, tweet us or simply complete the enquiry form.', 'brilia') %}</p>
                    </div>
                    <footer class="cell-footer">{{ wp.do_shortcode('[contact-form-7 id="1705" title="Request-A-Quote-Form"]') | raw }}</footer>
                </div>
            </div>
            <div class="col-sm-5 col-md-2">
                <div class="col-sm-10 cell cell-1 hidden-sm"></div>
                <figure class="col-sm-10 cell cell-2 cell-phone">
                    <div class="cell-icon"></div>
                    <figcaption class="cell-caption">
                        <h3 class="title sr-only">{% do wp._e('PHONE', 'brilia') %}</h3>
                        <p><a href="tel:6138662177">613 866 2177</a></p>
                    </figcaption>
                </figure>
                <figure class="col-sm-10 cell cell-2 cell-email">
                    <div class="cell-icon"></div>
                    <figcaption class="cell-caption">
                        <h3 class="title sr-only">{% do wp._e('EMAIL', 'brilia') %}</h3>
                        <p><a href="{{ wp.antispambot('mailto:hello@briliacreative.com') | raw }}">{{ wp.antispambot('hello.briliacreative.com') | raw }}</a></p>
                    </figcaption>
                </figure>
            </div>
        </section>
    </main>
    {% do wp.get_template_part('footer') %}
{% endspaceless %}