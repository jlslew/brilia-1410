var fs = require('fs');

function concat(opts) {
    var out = opts.src.map(function(filePath) {
        return fs.readFileSync(filePath, 'utf-8');
    });
    fs.writeFileSync(opts.dest, out.join('\n'), 'utf-8');
}

concat({
    src: [
        'js/transition.js',
//      'js/alert.js',
//      'js/button.js',
//      'js/carousel.js',
        'js/collapse.js',
//      'js/dropdown.js',
//      'js/modal.js',
//      'js/tooltip.js',
//      'js/popover.js',
//      'js/scrollspy.js',
//      'js/tab.js',
//      'js/affix.js',
        'js/jquery.waypoints.min.js',
        'js/functions.js',
    ],
    dest: 'brilia.js'
});